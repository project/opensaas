<?php

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function opensaas_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'opensaas_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function opensaas_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}

/**
 * Implements hook_install_tasks().
 */
function opensaas_install_tasks(&$install_state) {
  $tasks = [
    'opensaas_module_install' => [
      'display_name' => t('Install Demo Content'),
      'type' => 'batch'
    ]
  ];
  return $tasks;
}

/**
 * Installs the demo content modules in a batch.
 *
 */

function opensaas_module_install() {
  \Drupal::service('module_installer')->install(['opensaas_demo'], TRUE);
}
